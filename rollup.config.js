/*
 * Rollup Configuration https://rollupjs.org/guide/en/
 */
import glob from 'glob';
import babel from 'rollup-plugin-babel';
import clear from 'rollup-plugin-clear';
import postcss from 'rollup-plugin-postcss';
import commonjs from 'rollup-plugin-commonjs';
import { terser } from 'rollup-plugin-terser';
import resolve from 'rollup-plugin-node-resolve';
const { banner } = require( './scripts/banner' );

const globals = {
    'prop-types': 'PropTypes',
    react: 'React',
    'react-dom': 'ReactDom'
};

const plugins = ( extractCSS, umd ) => [

    // https://github.com/DongShelton/rollup-plugin-clear#readme
    clear( {
        targets: [ 'dist' ]
    } ),

    // https://github.com/rollup/rollup-plugin-babel#readme
    babel( {
        exclude: 'node_modules/**'
    } ),

    // https://github.com/rollup/rollup-plugin-node-resolve#readme
    resolve(),

    // https://github.com/rollup/rollup-plugin-commonjs#readme
    commonjs(),

    // https://github.com/egoist/rollup-plugin-postcss#readme
    // Not used to generate css (see scripts/create-theme-css.js).
    // Removes the `import '../styles/index.css` from each component file,
    // which is needed to support css in Storybook.
    postcss( {
        inject: false,
        extract: false
    } )

];

const components = glob.sync( 'components/**/index.js' ).reduce( ( accum, filepath ) => {

    const dirs = filepath.split( '/' );

    const key = dirs[ dirs.length - 2 ];

    if ( key !== 'components' ) accum[ key ] = filepath;

    return accum;

}, {} );

export default [
    {
        // https://rollupjs.org/guide/en/#input
        input: 'components/index.js',
        output: {
            // https://rollupjs.org/guide/en/#outputbanneroutputfooter
            banner,
            // https://rollupjs.org/guide/en/#outputglobals
            globals,
            // https://rollupjs.org/guide/en/#outputformat
            format: 'umd',
            // https://rollupjs.org/guide/en/#outputname
            name: 'DS',
            // https://rollupjs.org/guide/en/#outputfile
            file: 'dist/umd/index.js'
        },
        // https://rollupjs.org/guide/en/#plugins
        plugins: [
            ...plugins( true ),
            // https://terser.org/
            terser( {
                output: {
                    preamble: banner
                }
            } )
        ],
        // https://rollupjs.org/guide/en/#external
        external: Object.keys( globals )
    },
    {
        // https://rollupjs.org/guide/en/#input
        input: 'components/index.js',
        // https://rollupjs.org/guide/en/#plugins
        plugins: plugins( true ),
        // https://rollupjs.org/guide/en/#external
        external: Object.keys( globals ),
        output: [
            {
                // https://rollupjs.org/guide/en/#outputbanneroutputfooter
                banner,
                // https://rollupjs.org/guide/en/#outputdir
                dir: 'dist/esm',
                // https://rollupjs.org/guide/en/#outputformat
                format: 'esm'
            },
            {
                // https://rollupjs.org/guide/en/#outputbanneroutputfooter
                banner,
                // https://rollupjs.org/guide/en/#outputdir
                dir: 'dist/cjs',
                // https://rollupjs.org/guide/en/#outputformat
                format: 'cjs'
            }
        ]
    },
    {
        // https://rollupjs.org/guide/en/#input
        input: components,
        // https://rollupjs.org/guide/en/#plugins
        plugins: plugins(),
        // https://rollupjs.org/guide/en/#external
        external: Object.keys( globals ),
        output: [
            {
                // https://rollupjs.org/guide/en/#outputbanneroutputfooter
                banner,
                // https://rollupjs.org/guide/en/#outputdir
                dir: 'dist/esm',
                // https://rollupjs.org/guide/en/#outputformat
                format: 'esm'
            },
            {
                // https://rollupjs.org/guide/en/#outputbanneroutputfooter
                banner,
                // https://rollupjs.org/guide/en/#outputdir
                dir: 'dist/cjs',
                // https://rollupjs.org/guide/en/#outputformat
                format: 'cjs'
            }
        ]
    }
];