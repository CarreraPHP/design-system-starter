const pkg = require( '../package.json' );

const banner = `/* ${ pkg.name } v${ pkg.version } © 2019 ${ pkg.author } */`;

exports.banner = banner;
exports.default = banner;