import { create } from '@storybook/theming';

// https://github.com/storybookjs/storybook/blob/master/docs/src/pages/configurations/theming/index.md
export default create( {
    base: 'light',
    brandTitle: 'Design System',
    colorSecondary: '#0A41CF',
    barTextColor: '#2C2C2C',
    barSelectedColor: '#0A41CF'
} );