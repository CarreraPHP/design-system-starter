const path = require( 'path' );
const createCompiler = require( '@storybook/addon-docs/mdx-compiler-plugin' );
const babelrc = require( '../babel.config' );

const root = path.resolve( __dirname, '../' );

module.exports = async ( { config } ) => {

    // Remove conflicting css rule
    // https://github.com/storybookjs/storybook/issues/6319#issuecomment-477852640
    config.module.rules = config.module.rules.filter( ( rule ) => rule.test.toString() !== /\.css$/.toString() );

    config.module.rules = [

        ...config.module.rules,

        {
            test: /\.mdx$/,
            exclude: [
                /node_modules/
            ],
            use: [
                {
                    // https://github.com/babel/babel-loader#readme
                    loader: 'babel-loader',
                    options: {
                        // https://babeljs.io/docs/en/options#root
                        root: './',
                        plugins: [
                            // https://babeljs.io/docs/en/next/babel-plugin-transform-react-jsx.html
                            '@babel/plugin-transform-react-jsx',
                            // https://github.com/tleunen/babel-plugin-module-resolver#readme
                            [ 'module-resolver',
                                {
                                    // https://github.com/tleunen/babel-plugin-module-resolver/blob/master/DOCS.md#cwd
                                    cwd: 'packagejson',
                                    // https://github.com/tleunen/babel-plugin-module-resolver/blob/master/DOCS.md#alias
                                    alias: {
                                        '@mdx-js': './storybook/node_modules/@mdx-js',
                                        '@storybook': './storybook/node_modules/@storybook'
                                    }
                                },
                                'storybook-settings'
                            ]
                        ]
                    }
                },
                {
                    // https://mdxjs.com/getting-started/webpack
                    loader: '@mdx-js/loader',
                    options: {
                        compilers: [ createCompiler( {} ) ]
                    }
                }
            ]
        },

        {
            test: /\.css$/,
            use: [
                // https://github.com/webpack-contrib/style-loader#readme
                'style-loader',
                {
                    // https://github.com/webpack-contrib/css-loader#readme
                    loader: 'css-loader',
                    options: {
                        importLoaders: 1
                    }
                },
                {
                    // https://github.com/postcss/postcss-loader#readme
                    loader: 'postcss-loader',
                    options: {
                        config: {
                            path: root
                        }
                    }
                }
            ]
        },

        {
            test: /.js$/,
            exclude: [
                /node_modules/
            ],
            // https://github.com/babel/babel-loader#readme
            loader: 'babel-loader',
            options: babelrc
        }
    ];


    return config;

};