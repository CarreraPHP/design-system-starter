import { configure, addParameters } from '@storybook/react';
import { DocsPage, DocsContainer } from '@storybook/addon-docs/blocks';
import theme from './theme';

addParameters( {
    // https://github.com/storybookjs/storybook/blob/master/docs/src/pages/configurations/options-parameter/index.md
    options: {
        theme: theme,
        panelPosition: 'right'
    },
    docs: {
        page: DocsPage,
        container: DocsContainer
    }
} );

configure( [
    require.context( '../docs', true, /\.mdx$/ ),
    require.context( '../components', true, /\.mdx$/ )
], module );