const path = require( 'path' );
const { existsSync } = require( 'fs' );

const root = path.resolve( __dirname );
const defaultTheme = path.resolve( root, './library/styles/themes/index.css' );
const hasDefaultTheme = existsSync( defaultTheme );

module.exports = ( ctx ) => {

    return {
        plugins: {

            // https://github.com/postcss/postcss-import#readme
            'postcss-import': {

                // https://github.com/postcss/postcss-import#root
                root
            },

            // https://github.com/csstools/postcss-preset-env#readme
            'postcss-preset-env': {

                // https://github.com/csstools/postcss-preset-env#autoprefixer
                autoprefixer: {
                    grid: true
                },

                // https://github.com/csstools/postcss-preset-env#importfrom
                // Needed for Storybook. It's overwritten during `npm run dist`.
                importFrom: hasDefaultTheme ? defaultTheme : null,

                // https://github.com/csstools/postcss-preset-env#features
                // https://github.com/csstools/postcss-preset-env/issues/32#issuecomment-393754060
                features: {

                    // https://github.com/postcss/postcss-custom-properties#preserve
                    'custom-properties': {
                        preserve: false
                    }
                }
            },

            // https://github.com/postcss/postcss-url#readme
            'postcss-url': {

                // https://github.com/postcss/postcss-url#basepath
                basePath: root,

                // https://github.com/postcss/postcss-url#url
                url: 'inline'
            },

            // https://github.com/cuth/postcss-pxtorem#readme
            'postcss-pxtorem': {},

            // https://cssnano.co/guides/optimisations
            cssnano: {

                // https://cssnano.co/guides/presets/
                preset: [ 'default', {
                    zindex: {
                        exclude: true
                    }
                } ]
            },

            'postcss-reporter': {}
        }
    };

};