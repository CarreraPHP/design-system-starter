import './styles/index.css';
import React, { useEffect, useState } from 'react';
import Button from '../button';

/**
 * Clicker
 *
 * @returns {HTMLElement}
 */
const Clicker = () => {

    const [ count, setCount ] = useState( 0 );

    function updateCount() {

        setCount( count + 1 );

    }

    useEffect( () => {

        document.title = `You clicked ${ count } times`;

    } );

    return (
        <div>
            <p className='msg'>You clicked { count } { count === 1 ? 'time' : 'times' }</p>
            <Button text='Click Me!' onClick={ updateCount } />
        </div>
    );

};

export default Clicker;