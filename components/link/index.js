import './styles/index.css';
import React from 'react';
import { string } from 'prop-types';

/**
 * Link
 *
 * @param {String} href - Link destination.
 * @param {String} text - The text to display.
 *
 * @returns {HTMLElement}
 */
const Link = ( { text, href } ) => {

    return (
        <a className='link' href={ href }>{ text }</a>
    );

};

Link.propTypes = {
    /** Link destination */
    href: string.isRequired,
    /** The text to display */
    text: string.isRequired
};

export default Link;